// console.log("Hello World");
// [SECTION] Arrays - used to store multiple related values in a single variable. It is declared using [] also known as "Array Literals"

// single variable
let studentNameA = '2020-1923';
let studentNameB = '2020-1924';
let studentNameC = '2020-1925';
let studentNameD = '2020-1926';


// declaration of an array
// syntax --> let/const arrayName = [elementA, elementB,....];

let studNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926'];

// EXAMPLES
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// POSSIBLE USE OF AN ARRAY BUT NOT RECOMMENDED
let mixedArr = [12, 'Asus', null, undefined, {}];


console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// console.log(typeof grades);

// Alternative way of writing arrays
let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];


// Create an array with values from variable
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";
let cities = [city1, city2, city3];
console.log(myTasks);
console.log(cities);

// [SECTION] LENGth PROPERTY - counts the data in the array
// syntax --> .lenght
let fullName = "Jamie Noble"; 
console.log(fullName.length);
console.log(cities.length);


// used to .lenght-1 to delete the last item in an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// deleting specific item 

// Another example using decrementation -- to delete in an array
cities.length--;
console.log(cities);

// we can't do same on strings, however
fullName.length = fullName.length-1
console.log(fullName.length);
fullName.length--;
console.log(fullName); 

// adding an item in an array using incrementation ++
let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++;
theBeatles.length++;
theBeatles.length++;
console.log(theBeatles);

// [SECTION] Reading from arrays
// Accessing array elements is one of the more common tasks that we do with an array.
// we access a specific data in an array using INDEX
// each element in an array is associated with it's own index/number
console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]);   
let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegends[1]);
console.log(lakersLegends[4]);
console.log(lakersLegends[-1]);

// you can also save/store array items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// You can also REASSIGN array values using the items' indices
console.log('Array before re-assignment');
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

// Accessing the LAST ELEMENT of an array
let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

// you can also add it directly
console.log(bullsLegends[bullsLegends.length-1]);

// ADDING items to an array
// USing indices, you can also add items into the array
let newArr = [];
console.log(newArr[0]);
newArr[0] = "Cloud Strife";
console.log(newArr);
console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);


// ADDING AFTER THE ARRAYS to the end list of the array  = .length
newArr[newArr.length] = "Barett Wallace";
console.log(newArr);


// LOOPING OVER AN ARRAY
// You can use a for loop to iterate over all items in an array
// set the counter as the index and set the condition that as long as the current index iterated is less than the length of the array
for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

// index is equal to 0 and it is less than the lenght, and we put incrementation index = 1, 


let numArr = [5, 12, 30, 46, 40, 11, 9, 3];
for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}
	else{
		console.log(numArr[index] + " is not divisible by 5")
	}
}


// [SECTION] Multi dimensional array
// used for storing complex data structure

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.log(chessBoard);
console.log(chessBoard[1][4]);
// [1] = column 
// [4] = row
// remember - counting starts with 0

console.log(chessBoard[4][3]);
console.table(chessBoard);
console.log("Pawn moves to: " + chessBoard[1][5]);